import React from 'react';
import PropTypes from 'prop-types';
export const Modal = ({header,closeIcon,actions,text,close}) => {
    return(
        <div className="modal">
            <header>
                {header}
                {closeIcon && <button onClick = {close} className="close-btn">x</button>}
               
            </header>
            <p className="modal-text">{text}</p>
       {actions}
        </div>
    )
}
export default Modal;