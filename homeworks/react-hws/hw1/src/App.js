import React, {useState} from 'react';
import Button from './components/Button'
import {Modal} from './components/Modal'

function App() {
  const [firstModalStatus,setFirstModalStatus] = useState(false);
  const [secondModalStatus,setSecondModalStatus] = useState(false);

  const toggleFirstModel = () =>setFirstModalStatus(v =>!v)

  return (
    <div className="App">
    <Button  backgroundColor='#b3382c'
     text='Open first modal window'
     onClick={toggleFirstModel}/>

    <Button  backgroundColor='#b3382c'
     text='Open second modal window'
     onClick={() =>alert('second modal window')}/>
     
     {firstModalStatus &&(
        <Modal  header='Do you want to delete this file?'
        close={toggleFirstModel}
     closeIcon={true}
    
     actions={[
     <Button 
      key={1}
       backgroundColor='rgba(0,0,0,.2)'
     text='Ok'
     onClick={() =>alert('delete file')}/>,

<Button
    key={2}
  backgroundColor='rgba(0,0,0,.2)'
     text='Cancel'
     onClick={() =>alert('cancelled')}/> 
      ]}/>
     )}
      </div>
     
     );
}

export default App;
