import React from 'react';

import './App.css';

function App() {
  return (
    <div className="App">
     
      <header>
         <div className="header">
       <ul className='logos'>
         <li className="logos-li"><img src={require("./imgs/facebook-top-icon@1X.png")} /> </li>
         <li className="logos-li"><img src={require("./imgs/top-dribble-icon@1X.png")}/></li>
         <li className="logos-li"><img src={require("./imgs/top-twitter-icon@1X.png")}/></li>
         <li className="logos-li"><img src={require("./imgs/top-mail-icon@1X.png")}/></li>
         <li className="logos-li"><img src={require("./imgs/top-vimeo-icon@1X.png")}/></li>

       </ul>

       <p className="login-txt">Login/Register</p>
       <button className='cart'> <img src={require("./imgs/cart@1X.png")}/>Cart (0)</button>

</div>
     </header>
     <section className='section1'>

     <div className="main-logo-div">

       <img className='disc-photo' src={require("./imgs/Vector Smart Object@1X.png")}/>
       <p className='m-letter'>M</p>
       <h2 className="store-txt">Store</h2>
       </div>

       <ul className='store'>
         <li className="list">HOME</li>
         <li className="list">CD's</li>
     <li className="list"> DVD’s  </li>     
     <li className="list"> NEWS </li>     
     <li className="list">  PORTFOLIO   </li>     
     <li className="list">CONTACT US</li>
       </ul>
     </section>
     <section className='section2'>
       <img className='border-imgs' src={require("./imgs/image1@1X.png")}/>
       <div className='arrows left'></div>
<img className='border-imgs' src={require("./imgs/s1-940x470@1X.png")}/>
<div className="img-txt">MUSIC EVENT WITH DJ STARTING AT 20.00 ON AUGUST 15TH</div>
<img className='border-imgs'src={require("./imgs/image1@1X.png")}/>
<div className='arrows right'></div>
     </section>
     <hr/>
     <hr/>
     <hr/>
     <h1 >WELCOME TO  <span className='musica-span'>MUSICA, </span> CHECK OUR LATEST ALBUMS</h1>
     <hr/>
     <hr/>
     <hr/>
   <table className='table'>
     <tr>
       <td><h4>CHECK OUR CD COLLECTION</h4>
       <p>Donec pede justo, fringilla vel, al, vulputate 
eget, arcu. In enim justo, lorem ipsum.
</p>  </td> </tr>
<tr>
  <td><h4>LISTEN BEFORE PURCHASE</h4>
  <p>Donec pede justo, fringilla vel, al, vulputate 
eget, arcu. In enim justo, lorem ipsum.
</p></td></tr>
<tr>
  <td><h4>UPCOMING EVENTS</h4>
  <p>Donec pede justo, fringilla vel, al, vulputate 
eget, arcu. In enim justo, lorem ipsum.
</p></td>
</tr>
   </table>
  
  
  <div style={{display:'flex',justifyContent:'center'}}> 
   <h2>LATEST ARRIVALS IN MUSICA</h2>
   <div className='arrivals_hrs'> <hr/> <hr/><hr/></div>
   <button className='arrow-btns'></button><button className='arrow-btns'></button>
</div>

<div style={{display:'flex',justifyContent:'center'}}> 
   <h2>ALBUMS CURRENTLY ON SALE</h2>
   <div className='arrivals_hrs'> <hr/> <hr/><hr/></div>
   <button className='arrow-btns'></button><button className='arrow-btns'></button>
</div>
<div style={{display:'flex',justifyContent:'center',alignItems:'center'}}>
   <h2>OUR MOST IMPORTANT PUBLISHERS   </h2>
   <div className="publishers_hrs"> <hr/><hr/><hr/></div></div>
   <ul className='publishers'>
     <li><img src={require('./imgs/waterLogo@1X.png')}/>
    <h4 className='watermelon'> Water<span>Melocious</span></h4></li>
    <li><img src={require('./imgs/plantcloud@1X.png')}/></li>
    <li><img src={require('./imgs/inspired@1X.png')}/>
    <p>InspiredTemplate</p></li>
    <li><img src={require('./imgs/bird@1X.png')}/></li>
    <li><img src={require('./imgs/man@1X.png')}/></li>
    <li><img src={require('./imgs/S copy@1X.png')}/>
    <h3>Your<span style={{color:'#444343'}}>Logo</span></h3>
    </li>

   </ul>
   
   <footer >
     <table >
       <thead >
         <tr>
         <td>Little about us</td>
       <td>Our Archives</td>
       <td>Popular Posts</td>
       <td>Search our Site</td>
</tr>
       </thead>
       <tbody >
         <td className='last-txt'><p><span style={{fontWeight:'bold'}}>Sed posuere</span> consectetur  est at. 
Nulla vitae elit libero, a pharetra. 
Lorem ipsum dolor sit amet, conse
ctetuer adipiscing elit.</p>
<p>Socialize with us</p>
</td>
<td >
  <p>March 2012</p>
<hr className='last-hrs'/>
<p>February 2012</p>
<hr className='last-hrs'/>
<p>January 2012</p>
<hr className='last-hrs'/>
<p>December 2011</p>
</td>
<td>

  <div style={{display:'flex'}}>
  <img style={{width:'90px',height:'70px',border:'5px solid #28292a'}}
  src={require('./imgs/image@1X (1).png')}></img>
<p>Great Album<p>
  12 COMMENTS</p></p></div>
  <div style={{display:'flex'}}>
<img style={{width:'90px',height:'70px',border:'5px solid #28292a'}}
src={require('./imgs/baddreams.png')}></img>
<p>Great Album<p>
12 COMMENTS</p></p></div>
</td>
<td>
  <input type='text' className='search-input' placeholder="Enter Search..."></input>
  <h5>Tag Cloud</h5>
  <div className='tags'>
    <div>
    <div>Audio CD</div>
    <div>Video</div>
    <div>Playlist</div></div>
    <div>
    <div>Avantgarde</div>
    <div>Melancholic</div></div>
  </div>
</td>
       </tbody>
     </table>
   <div className='last-footer-div'>
  
<a href=''>Home  </a>   <a href=''> Portfolio  </a>   <a href=''>Sitemap</a> <a href="">   Contact</a>  
    
  <p>Musica @2013 by PremiumCoding | All Rights Reserved</p>
  </div>
   </footer>
    </div>
  );
}

export default App;
